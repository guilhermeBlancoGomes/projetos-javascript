// Canvas
const canvas = document.createElement('canvas');
const ctx = canvas.getContext('2d');
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Imagem de fundo

let bgReady = false;
const bgImage = new Image();
bgImage.onload = function () {
    bgReady = true;
};
bgImage.src = 'img/background.png';

// Imagem do Heroi

let heroiReady = false;
const heroiImage = new Image();

heroiImage.onload = function () {
    heroiReady = true;
};
heroiImage.src = 'img/person.png';

// Imagem do Montro

let monstroReady = false;
const monstroImage = new Image();

monstroImage.onload = function () {
    monstroReady = true;
};
monstroImage.src = 'img/pokebola.png';

// Objetos do jogo

const heroi = {
    velocidade: 256 //movimento em pixels por segundo
};
const monstro = {};
let monstrosCapturados = 0;

// Eventos do teclado

const keysDown = {};

window.addEventListener('keydown', function (e) {
    keysDown[e.keyCode] = true;
}, false);

window.addEventListener('keyup', function (e) {
    delete keysDown[e.keyCode];
}, false);

// Resetar o jogo assim que capturar o monstro

const reset = function () {
    heroi.x = canvas.width / 2;
    heroi.y = canvas.height / 2;

    // posiciona o monstro aleatoriamente na tela

    monstro.x = 32 + Math.random() * (canvas.width - 64);
    monstro.y = 32 + Math.random() * (canvas.height - 64);
};

// Atualiza os objetos do jogo

const update = function (modifier) {
    // 38 é o keyCode da tecla para cima 
    if (38 in keysDown) {
        heroi.y -= heroi.velocidade * modifier;
    }
    // 40 é o keyCode da tecla para baixo
    if (40 in keysDown) {
        heroi.y += heroi.velocidade * modifier;
    }
    // 37 é o keyCode da teclas para esquerda
    if (37 in keysDown) {
        heroi.x -= heroi.velocidade * modifier;
    }
    // 39 é o keyCode da teclas para direita
    if (39 in keysDown) {

        heroi.x += heroi.velocidade * modifier;
    }

    // saber se o heroi e o monstro se encontraram
    if (heroi.x <= monstro.x + 32 && monstro.x <= heroi.x + 32 && heroi.y <= monstro.y + 32 && monstro.y <= heroi.y + 32) {
        ++monstrosCapturados;
        reset();
    }
};
// renderizar tudo
const render = function () {
    if (bgReady) {
        ctx.drawImage(bgImage, 0, 0);
    }
    if (heroiReady) {
        ctx.drawImage(heroiImage, heroi.x, heroi.y);
    }
    if (monstroReady) {
        ctx.drawImage(monstroImage, monstro.x, monstro.y);
    }
    // Pontuação
    ctx.fillStyle = 'rgb(250,250,250)';
    ctx.font = '24px Arial';
    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';

    ctx.fillText('Pokemons capturados: ' + monstrosCapturados, 32, 32);
};

// controla o loop do jogo
const main = function () {
    const now = Date.now();
    const delta = now - then;

    update(delta / 1000);
    render();
    then = now;

    // executa o mais rápido possível
    requestAnimationFrame(main);
};

// Cross-browser para requestAnimationFrame
const w = window;
const requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

let then = Date.now();
reset();
main();
