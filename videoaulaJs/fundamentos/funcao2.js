// armazenando uma função em uma variavel

const imprimirSoma = function(a,b){
    console.log(a+b);
} 
imprimirSoma(3,6);

// armazenando uma função arrow em uma variavel
const soma = (a,b) =>{
    return a + b;
}
console.log(soma(4,3));

const dividir = (a,b) =>{
    return a / b ;
}
console.log(dividir(5,2));