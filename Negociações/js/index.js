// verificar cada campo da lista
const campos = [
    document.querySelector('.container--input--nome'),
    document.querySelector('.container--input--mes'),
    document.querySelector('.container--input--valor')
];

var tbody = document.querySelector('table tbody');
var inputValor = document.querySelector('.container--input--valor');

document.querySelector('.container--formulario').addEventListener('submit', function(event){
    
    event.preventDefault();
    var tr = document.createElement('tr');
    tr.setAttribute('class','container--formulario--tr');
   
          
    campos.forEach(function(campo){
        var td = document.createElement('td');
        td.textContent = campo.value;            
        tr.appendChild(td);
        
    });
    document.querySelector('.alert-success').style.display = 'block'; 
    
    tbody.appendChild(tr);
    
    campos[0].value = '';
    campos[1].value = '';
    campos[2].value = 'R$';

    campos[0].focus();
});


