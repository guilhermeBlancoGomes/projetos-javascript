import TodoList from './TodoList';
import React, {Component} from 'react';
import TodoForm from './TodoForm';

class Todo extends Component{
    state = {
        tasks: [
            // {id: 1, name: 'trabalhar', done: false},
            // {id: 2, name: 'estudar',   done: false},
            
        ]
    }
    addTask(e){
        e.preventDefault();
        const tasks = this.state.tasks;
        const newTask = { id: tasks.length + 1, name: e.target.inputForm.value, done: false }
        const addNewTask = tasks.concat(newTask);
        this.setState({
            tasks: addNewTask
        })
        e.target.inputForm.value = null;
    }
    render(){
        return(
            <div>
                <TodoList tasks={this.state.tasks} />
                <TodoForm add={this.addTask.bind(this)} />
            </div>
        )
    }

}
export default Todo;
