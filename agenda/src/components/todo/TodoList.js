import React from 'react';


const todoList = props =>{
    return(
        <ul>
            {
               props.tasks.map(item =>{
                  return  <li key={item.id}> { item.name } </li> 
                })
            }            
        </ul>
    )
}; 
export default todoList;