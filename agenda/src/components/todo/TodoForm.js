import React from 'react';
import './teste.css';
const todoForm = props =>{
    return(
        <form onSubmit={props.add}>
            <input type='text' name='inputForm' placeholder='Digite suas Tasks' minLength='3' className='inputForm' />
        </form>
    )
}
export default todoForm;