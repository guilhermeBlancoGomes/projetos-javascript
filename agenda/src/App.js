import React, {Component} from 'react';
import './App.css';
import Todo from './components/todo/Todo';

class App extends Component {
  render() {
    return (
      
      <div className="container">
        <Todo></Todo>
      </div>
    );
  }
}

export default App;
